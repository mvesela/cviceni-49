// eslint-disable-next-line import/no-extraneous-dependencies
const slugify = require('slugify');

const fs = require('fs');
const fsp = require('fs').promises;

function upperCaseFirst(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

function changeNameToCamelCase(str) {
  const stringArray = str.split('-');

  stringArray.forEach((part, index) => {
    stringArray[index] = upperCaseFirst(part);
  });

  return `${stringArray.join('')}`;
}

async function renameFolder(oldPath, newPath, successString, errorString) {
  try {
    await fsp.rename(oldPath, newPath);
    console.log('%c%s', 'color: #731d1d', successString);
  }
  catch (err) {
    console.log('%c%s', 'color: #f200e2', errorString);
  }
}

async function changeNameOfCviceni(oldNameSlug, newNameSlug, oldNameCamelCase, newNameCamelCase, newName) {
  // ======================================AUDIO===========================================
  // Rename audio
  const oldAudioPath = `./src/audio/${oldNameSlug}`;
  const newAudioPath = `./src/audio/${newNameSlug}`;
  const audioSucces = 'Successfully renamed the directory in audio folder.';
  const audioError = 'Folder in audio was not renamed. Reason: bad old name or there is not yet audio folder for this cviceni.';
  await renameFolder(oldAudioPath, newAudioPath, audioSucces, audioError);

  // ======================================DATA===========================================
  // Rename data file and the assets in it

  const oldDataPath = `./src/data/cviceni${oldNameCamelCase}.json`;
  const newDataPath = `./src/data/cviceni${newNameCamelCase}.json`;
  const dataSucces = 'Successfully renamed the directory in data folder.';
  const dataError = 'File in data folder was not renamed. Reason: bad old name or there is different slug.';
  // Change name in data and all path by old slug
  const data = fs.readFileSync(oldDataPath);
  const dataWithchanges = JSON.stringify(JSON.parse(data))
    .split(oldNameSlug)
    .join(newNameSlug);
  const newData = JSON.parse(dataWithchanges);

  newData.cviceni.nazev = newName;
  // fs.writeFile(oldDataPath, JSON.stringify(newData, null, 2), 'utf-8', (err) => {
  //   if (err) throw err;
  //   console.log('Data in json file was successfully updated.');
  //   // Rename the data file
  //   renameFolder(oldDataPath, newDataPath, dataSucces, dataError);
  // });
  await fsp.writeFile(oldDataPath, JSON.stringify(newData, null, 2), 'utf-8').then(async () => {
    console.log('Data in json file was successfully updated.');
    // Rename the data file
    await renameFolder(oldDataPath, newDataPath, dataSucces, dataError);
  }).catch((err) => {
    console.log(`Data in json file was NOT updated. ${err}`);
  });

  // ======================================IMG===========================================
  const oldImgPath = `./src/img/${oldNameSlug}`;
  const newImgPath = `./src/img/${newNameSlug}`;
  const imgSucces = 'Successfully renamed the directory in img folder.';
  const imgError = 'Folder in img was not renamed. Reason: bad old name or there is not yet img folder for this cviceni.';
  await renameFolder(oldImgPath, newImgPath, imgSucces, imgError);

  // ======================================PDF===========================================
  const oldPdfPath = `./src/pdf/doporuceny-postup/${oldNameSlug}.pdf`;
  const newPdfPath = `./src/pdf/doporuceny-postup/${newNameSlug}.pdf`;
  const pdfSucces = 'Successfully renamed the file in pdf folder.';
  const pdfError = 'File in pdf folder was not renamed. Reason: bad old name or there is not yet pdf file for this cviceni.';
  await renameFolder(oldPdfPath, newPdfPath, pdfSucces, pdfError);

  // ======================================TEXT===========================================
  const oldTextPath = `./src/text/${oldNameSlug}`;
  const newTextPath = `./src/text/${newNameSlug}`;
  const textSucces = 'Successfully renamed the directory in text folder.';
  const textError = 'Folder in text was not renamed. Reason: bad old name or there is not yet text folder for this cviceni.';
  await renameFolder(oldTextPath, newTextPath, textSucces, textError);

  // ======================================VIDEO===========================================
  const oldVideoPath = `./src/video/${oldNameSlug}`;
  const newVideoPath = `./src/video/${newNameSlug}`;
  const videoSucces = 'Successfully renamed the directory in video folder.';
  const videoError = 'Folder in video was not renamed. Reason: bad old name or there is not yet video folder for this cviceni.';
  await renameFolder(oldVideoPath, newVideoPath, videoSucces, videoError);

  // ======================================PUG===========================================
  const oldPugPath = `./src/views/cviceni/${oldNameSlug}`;
  const newPugPath = `./src/views/cviceni/${newNameSlug}`;
  const pugSucces = 'Successfully renamed the directory in pug folder.';
  const pugError = 'Foleder in pug was not renamed. Reason: bad old name or there is not yet pug folder for this cviceni.';

  // change loading pugData in slug (the name of data/json has changed)
  const pugData = fs.readFileSync(`${oldPugPath}/index.pug`, 'utf8');
  const pugDataWithchanges = pugData
    .split(`cviceni${oldNameCamelCase}`)
    .join(`cviceni${newNameCamelCase}`);

  await fsp.writeFile(`${oldPugPath}/index.pug`, pugDataWithchanges, 'utf-8').then(async () => {
    console.log('Data in pug file was successfully updated.');
    // Rename the data file
    await renameFolder(oldPugPath, newPugPath, pugSucces, pugError);
  }).catch((err) => {
    console.log(`Data in PUG file was NOT updated. ${err}`);
  });

}

async function renameCviceni(oldName, newName) {

  // await Promise.resolve();
  const oldNameSlug = slugify(oldName, {
    remove: /[!?#=*+~.()'":@]/g,
  }).toLowerCase();
  const newNameSlug = slugify(newName, {
    remove: /[!?#=*+~.()'":@]/g,
  }).toLowerCase();
  const oldNameCamelCase = changeNameToCamelCase(oldNameSlug);
  const newNameCamelCase = changeNameToCamelCase(newNameSlug);
  const oldDataPathForCheck = `./src/data/cviceni${oldNameCamelCase}.json`;
  const newDataPathForCheck = `./src/data/cviceni${newNameCamelCase}.json`;
  // try {
  if (!fs.existsSync(oldDataPathForCheck)) {
    console.error(`The given old filename: ${oldDataPathForCheck} is not valid!`);
    return '';
  }
  if (fs.existsSync(newDataPathForCheck)) {
    console.error(`The given new filename: ${newDataPathForCheck} is already taken!`);
    return '';
  }

  console.log('Changing cviceni:');
  await changeNameOfCviceni(oldNameSlug, newNameSlug, oldNameCamelCase, newNameCamelCase, newName).catch();
  return `cviceni${newNameCamelCase}.json`;
}

module.exports.renameCviceni = renameCviceni;
